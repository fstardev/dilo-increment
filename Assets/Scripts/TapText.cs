using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TapText : MonoBehaviour
{
    [SerializeField] private float spawnTime = .5f;
    [SerializeField] private Text text;

    private float m_spawnTime;

    public Text Text => text;

    private void OnEnable()
    {
        m_spawnTime = spawnTime;
    }

    private void Update()
    {
        m_spawnTime -= Time.unscaledDeltaTime;
        if (m_spawnTime <= 0f) 
        {
            gameObject.SetActive(false);
        }
        else
        {
            text.CrossFadeAlpha(0, .5f, false);
            if (text.color.a == 0)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
