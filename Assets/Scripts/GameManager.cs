using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }
    
    
    [Serializable]
    public struct ResourceConfig
    {
        public string name;
        public double unlockCost;
        public double upgradeCost;
        public double output;
    }

    [SerializeField] [Range(0, 1f)] private float autoCollectPercentage = .1f;
    [SerializeField] private ResourceConfig[] resourceConfigs;
    [SerializeField] private Sprite[] resourceSprites;

    [SerializeField] private Transform resourcesParent;
    [SerializeField] private ResourceController resourcePrefab;
    [SerializeField] private TapText tapTextPrefab;

    [SerializeField] private Transform coinIcon;
    [SerializeField] private Text goldInfo;
    [SerializeField] private Text autoCollectInfo;

    private readonly List<ResourceController> m_activeResources = new List<ResourceController>();
    private readonly List<TapText> m_tapTextPool = new List<TapText>();
    private float m_collectSecond;

    public double TotalGold { get; private set; }

    private void Start()
    {
        AddAllResources();
    }

    private void Update()
    {
        m_collectSecond += Time.unscaledDeltaTime;
        if (m_collectSecond >= 1f)
        {
            CollectPerSecond();
            m_collectSecond = 0;
        }

        CheckResourceCost();

        coinIcon.localScale = Vector3.LerpUnclamped(coinIcon.transform.localScale, Vector3.one * 2f, .15f);
        coinIcon.transform.Rotate(0, 0, Time.deltaTime * -100f);
       

    }

    private void AddAllResources()
    {
        var showResource = true;
        foreach (var config in resourceConfigs)
        {
            var obj = Instantiate(resourcePrefab.gameObject, resourcesParent, false);
            var resource = obj.GetComponent<ResourceController>();
            
            resource.SetConfig(config);
            obj.gameObject.SetActive(showResource);
            if (showResource && !resource.IsUnlocked)
            {
                showResource = false;
            }
            m_activeResources.Add(resource);
        }
    }

    private void AddOutput(double value)
    {
        TotalGold += value;
        goldInfo.text = $"Gold: {TotalGold:0}";
        CheckCollectedGolds(TotalGold);
    }

   

    public void AddGold(double value)
    {
        TotalGold += value;
        goldInfo.text = $"Gold: {TotalGold:0}";
        CheckCollectedGolds(TotalGold);
    }
    
    private static void CheckCollectedGolds(double gold)
    {
        AchievementController.Instance.UnlockAchievementType(AchievementController.AchievementType.CollectMoney,
            gold);
    }


    private void CollectPerSecond()
    {
        
        double output = m_activeResources.Where(resource => resource.IsUnlocked)
            .Aggregate(0, (current, resource) => (int) (current + resource.GetOutput()));
        
        output *= autoCollectPercentage;
        autoCollectInfo.text = $"Auto Collect: {output:F1}/second";
        
        AddOutput(output);

    }
    public void CollectByTap(Vector3 tapPosition, Transform parent)
    {
       // var output = m_activeResources.Sum(resource => resource.GetOutput());
        double output = m_activeResources.Where(resource => resource.IsUnlocked)
           .Aggregate(0, (current, resource) => (int) (current + resource.GetOutput()));
        var tapText = GetOrCreateTapText();
        var tapTransform = tapText.transform;
        tapTransform.SetParent(parent, false);
        tapTransform.position = tapPosition;

        tapText.Text.text = $"+{output:0}";
        tapText.gameObject.SetActive(true);
        coinIcon.transform.localScale = Vector3.one * 1.75f;

        AddGold(output);
    }
    
    private TapText GetOrCreateTapText()
    {
        var tapText = m_tapTextPool.Find(t => !t.gameObject.activeSelf);
        if (tapText == null)
        {
            tapText = Instantiate(tapTextPrefab).GetComponent<TapText>();
            m_tapTextPool.Add(tapText);
        }

        return tapText;
    }
    
    private void CheckResourceCost()
    {
        foreach (var resource in m_activeResources)
        {
            var isBuyAble = false;
            if (resource.IsUnlocked)
            {
                isBuyAble = TotalGold >= resource.GetUpgradeCost();
            }
            else
            {
                isBuyAble = TotalGold >= resource.GetUnlockCost();
            }
            resource.ResourceImage.sprite = resourceSprites[isBuyAble ? 1 : 0];
        }
    }

    public void ShowNextResource()
    {
        foreach (var resource in m_activeResources.Where(resource => !resource.gameObject.activeSelf))
        {
            resource.gameObject.SetActive(true);
            break;
        }
    }
}
