using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceController : MonoBehaviour
{
     [SerializeField] private Image resourceImage;
     
     [SerializeField] private Text resourceDescription;
     [SerializeField] private Text resourceUpgradeCost;
     [SerializeField] private Text resourceUnlockCost;

     private GameManager.ResourceConfig m_config;
     private int m_level = 1;
     private Button m_button;

     public Image ResourceImage => resourceImage;
     public bool IsUnlocked { get; private set; }

     private void Awake()
     {
          m_button = GetComponent<Button>();
     }

     private void Start()
     {
          m_button.onClick.AddListener(() =>
          {
               if (IsUnlocked)
               {
                    UpgradeLevel();
               }
               else
               {
                    UnlockResource();
               }
          });
     }

   

     public void SetConfig(GameManager.ResourceConfig config)
     {
          m_config = config;
          resourceDescription.text = $"{m_config.name} Lv.{m_level}\n+{GetOutput():0}";
          resourceUnlockCost.text = $"Unlock Cost\n{GetUnlockCost():0}";
          resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost():0}";

          SetUnlocked(m_config.unlockCost == 0);
     }

     private void UnlockResource()
     {
          var unlockCost = GetUnlockCost();
          if(GameManager.Instance.TotalGold < unlockCost) return;
          
          SetUnlocked(true);
          GameManager.Instance.ShowNextResource();

          AchievementController.Instance.UnlockAchievementType(AchievementController.AchievementType.UnlockResource,
               m_config.name);
     }

     private void UpgradeLevel()
     {
          var upgradeCost = GetUpgradeCost();
          if(GameManager.Instance.TotalGold < upgradeCost) return;
          GameManager.Instance.AddGold(-upgradeCost);
          m_level++;

          resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";
          resourceDescription.text = $"{m_config.name} Lv. {m_level}\n+{GetOutput():0}";
     }
     private void SetUnlocked(bool unlocked)
     {
          IsUnlocked = unlocked;
          resourceImage.color = IsUnlocked ? Color.white : Color.grey;
          resourceUnlockCost.gameObject.SetActive(!unlocked);
          resourceUpgradeCost.gameObject.SetActive(unlocked);
     }

     public double GetOutput()
     {
          return m_config.output * m_level;
     }

     public double GetUpgradeCost()
     {
          return m_config.upgradeCost * m_level;
     }

     public double GetUnlockCost()
     {
          return m_config.unlockCost;
     }
}
