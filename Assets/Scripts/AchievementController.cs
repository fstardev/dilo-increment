using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AchievementController : MonoBehaviour
{
    private static AchievementController _instance;

    public static AchievementController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<AchievementController>();
            }

            return _instance;
        }
    }

    [Serializable]
    public class AchievementData
    {
        public string title;
        public AchievementType type;
        public string value;
        public double gold;
        public bool isUnlocked;
    }

    public enum AchievementType
    {
        UnlockResource,
        CollectMoney
    }

    [SerializeField] private Transform popUpTransform;
    [SerializeField] private Text popUpText;
    [SerializeField] private float popUpShowDuration = 3f;
    [SerializeField] private List<AchievementData> achievements;

    private float m_popUpShowDurationCounter;

    private void Update()
    {
        if (m_popUpShowDurationCounter > 0)
        {
            m_popUpShowDurationCounter -= Time.unscaledDeltaTime;
            popUpTransform.localScale = Vector2.LerpUnclamped(popUpTransform.localScale, Vector2.one, .5f);
        }
        else
        {
            popUpTransform.localScale = Vector2.LerpUnclamped(popUpTransform.localScale, Vector2.right, .5f);
        }
    }

    public void UnlockAchievementType(AchievementType type, string value)
    {
        var achievement = achievements.Find(a => a.type == type && a.value == value);
        ShowAchievement(achievement);
    }
    
    public void UnlockAchievementType(AchievementType type, double gold)
    {
        var achievement = achievements.Find(a => a.type == type && !a.isUnlocked  && a.gold < gold);
        ShowAchievement(achievement);
    }

    private void ShowAchievement(AchievementData achievement)
    {
        if (achievement is {isUnlocked: false})
        {
            achievement.isUnlocked = true;
            ShowAchievementPopUp(achievement);
        }
    }

  


    private void ShowAchievementPopUp(AchievementData achievement)
    {
        popUpText.text = achievement.title;
        m_popUpShowDurationCounter = popUpShowDuration;
        popUpTransform.localScale = Vector2.right;
    }
}
